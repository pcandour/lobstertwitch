const lib = require('./twitchlib.js');
const sinon = require("sinon");

// To add more tests, add extra methods with names starting 'test' to the TestRunner.

let Client = class {
  constructor() {
    this.messages = [];
  }

  say(target, msg) {
    this.messages.push({'target': target, 'msg': msg});
  }

  reset() {
    this.messages = [];
  }
};

let TestRunner = class {
  constructor() {
    this.client = new Client();
    lib.setClient(this.client);
    this.setUp("UNKNOWN");
  }

  setUp(testMethodName) {
    this.client.reset();
    this.testMethodName = testMethodName;
    this.context = {"display-name": "user"};
  }

  colourTitle() {
    return '\u001b[' + 32 + 'm[' + this.testMethodName + ']\u001b[0m';
  }

  assertIntEqual(expected, actual, i=null) {
    const title = this.colourTitle();
    let iMsg = "";
    if (i != null) {
      iMsg = `${i} `;
    }

    if (expected !== actual) {
      console.error(`${title}: ${iMsg}Expected ${expected}`);
      console.error(`${title}: ${iMsg}Actual   ${actual}`);
      return true;
    }
  }

  assertStrEqual(expected, actual, i=null) {
    const title = this.colourTitle();
    let iMsg = "";
    if (i != null) {
      iMsg = `${i} `;
    }

    if (expected !== actual) {
      console.error(`${title}: ${iMsg}Expected ${expected}`);
      console.error(`${title}: ${iMsg}Actual   ${actual}`);
      return true;
    }
  }

  assertStrBegins(expected, actual, i=null) {
    const title = this.colourTitle();
    let iMsg = "";
    if (i != null) {
      iMsg = `${i} `;
    }

    if (!actual.startsWith(expected)) {
      console.error(`${title}: ${iMsg}Expected StartsWith ${expected}`);
      console.error(`${title}: ${iMsg}Actual              ${actual}`);
      return true;
    }
  }

  checkMessages(expected) {
    let error = false;
    const actual = this.client.messages;
    const title = this.colourTitle();
  
    if (expected.length != actual.length)
    {
      console.error(`${title}: Expected length ${expected.length} != ${actual.length}`);
      error = true;
    }
  
    for (let i = 0; i < Math.min(expected.length, actual.length); i++) {
      const expectedLine = expected[i];
      const actualLine = actual[i];
      
      if (this.assertStrEqual(expectedLine['target'], actualLine['target'], i)) {
        error = true;
      }
      if (this.assertStrEqual(expectedLine['msg'], actualLine['msg'], i)) {
        error = true;
      }
    }
  
    if (error === true) {
      console.error(`${title} Test failed`);
    }
    else {
      console.log(`${title} Test Passed`);
    }
  
    return error;
  }

  testSay() {
    this.client.say("channel", "test");
    this.checkMessages([{'target': 'channel', 'msg': 'test'}]);
  }

  testJoinSpecialPerson() {
    lib.setVips(['specialperson']);
    lib.onJoinHandler("channel", "specialperson", true);
    this.checkMessages([{'target': 'channel', 'msg': 'Hey specialperson!'}]);
  }

  testChance() {
    sinon.replace(lib, "rollPerc", sinon.fake.returns(100));
    lib.onMessageHandler("channel", this.context, "!chance", false);
    sinon.restore();
    this.checkMessages([{'target': 'channel', 'msg': 'user: The chance is 100%.'}],);
  }

  testChanceThing() {
    sinon.replace(lib, "rollPerc", sinon.fake.returns(100));
    lib.onMessageHandler("channel", this.context, "!chance thing", false);
    sinon.restore();
    this.checkMessages([{'target': 'channel', 'msg': 'user: The chance of thing is 100%.'}]);
  }

  testNull() {
    lib.onMessageHandler("channel", this.context, "Some random message", false);
    this.checkMessages([]);
  }
}

lib.onConnectedHandler("host", 1234);
const testRunner = new TestRunner();

const getTestMethods = () => Object.getOwnPropertyNames(TestRunner.prototype)
  .filter(item => typeof TestRunner.prototype[item] === 'function')
  .filter(item => item.startsWith('test'));

testRunner.setUp();

getTestMethods().forEach(testMethod => {
  testRunner.setUp(testMethod);
  testRunner[testMethod]();
});