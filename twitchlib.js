let client = null;
let vips = [];

const self = {
  setClient: function (newClient) {
    client = newClient;
  },

  setVips: function (newVips) {
    vips = newVips;
  },

  // Returns an int between 0 and the number inclusive
  rollPerc: function(max) {
    return Math.floor(Math.random() * (max+1));
  },

  // Called every time the bot connects to Twitch chat
  onConnectedHandler: function (addr, port) {
    console.log(`* Connected to ${addr}:${port}`);
  },

  // Called every time a message comes in
  // target: the channel it was from
  // context: The user it was from https://dev.twitch.tv/docs/irc/tags#userstate-twitch-tags
  // msg: The contents of the message
  // fromSelf: Whether it came from the bot
  onMessageHandler: function (target, context, msg, fromSelf) {
    if (fromSelf) { return; }

    if (msg.startsWith('!chance')) {
      if (msg.startsWith('!chance')) {
        msg = msg.substring(8);
      }

      const perc = self.rollPerc(100);

      if (msg.trim() != "") {
        msgLower = msg.toLowerCase();
        if (msgLower.startsWith('i ')) {
          msg = 'you %s' % msg.substring(2);
        }
        if (msgLower.startsWith('my ')) {
          msg = 'your %s' % msg.substring(3);
        }
        msg = msg.replace(' i ', ' you ').replace(' I ', ' you ').replace(' my ', ' your ')
        
        client.say(target, `${context['display-name']}: The chance of ${msg} is ${perc}%.`)
      }
      else {
        client.say(target, `${context['display-name']}: The chance is ${perc}%.`)
      }
    }
  },

  onJoinHandler: function (target, nick, authed) {
    if (authed && vips.indexOf(nick.toLowerCase()) !== -1) {
      client.say(target, `Hey ${nick}!`);
    }
  }
};

module.exports = self;