const tmi = require('tmi.js');
const lib = require('./twitchlib.js');

// Define configuration options
// Full instructions at https://dev.twitch.tv/docs/irc
const opts = {
  identity: {
    // The username for your bot that you created
    username: "BOTUSERNAME",
    // The OAUTH token from https://twitchapps.com/tmi/
    password: "oauth:YOURTWITCHAPIKEY",
  },
  channels: [
    // A list of channels for your bot to join
    "CHANNELTOJOIN"
  ]
};

// Create a client with our options
const client = new tmi.client(opts);
lib.setClient(client);
// List of VIP names. Currently used to automatically say hello when they join.
lib.setVips(['someperson', 'someotherperson']);

// Register our event handlers
// You can get a complete list by looking for this.emits in
// https://github.com/tmijs/tmi.js/blob/master/lib/client.js
client.on('message', lib.onMessageHandler);
client.on('connected', lib.onConnectedHandler);
client.on('join', lib.onJoinHandler);

// Connect to Twitch:
client.connect();
