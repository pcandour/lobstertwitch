# Lobsterbot Twitch bot

This is a pretty simple example Twitch bot based on the example from Twitch, but demonstrating a few extra pieces and
also including a working test suite.

## Usage

git clone somewhere and change the settings in twitchbot.js and then:

```
sudo snap install node
npm install tmi
node twitchbot.js
```

## Testing

```
npm install sinon
node testbot.js
```
